﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomScript : MonoBehaviour {

    float speed = 2f;

    Vector3 targetPosition = new Vector3(0.67f, 0.4f, 21.74f);

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

        float step = speed * Time.deltaTime;
        this.transform.position = Vector3.MoveTowards(this.transform.position, targetPosition, step);
    }
}
