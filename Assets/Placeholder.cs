﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using UnityEngine;
using HoloToolkit.Unity;

public class Placeholder : MonoBehaviour, IInputClickHandler
{
    public Transform prefab;
    bool loaded;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        var instance = Instantiate(this.prefab);

        instance.gameObject.transform.position =
          GazeManager.Instance.GazeOrigin +
          GazeManager.Instance.GazeNormal * 1.5f;
    }

    // Use this for initialization
    void Start () {
        InputManager.Instance.PushFallbackInputHandler(
      this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        if (!this.loaded && (WorldAnchorManager.Instance.AnchorStore != null))
        {
            var ids = WorldAnchorManager.Instance.AnchorStore.GetAllIds();

            // NB: I'm assuming that the ordering here is either preserved or
            // maybe doesn't matter.
            foreach (var id in ids)
            {
                var instance = Instantiate(this.prefab);
                WorldAnchorManager.Instance.AttachAnchor(instance.gameObject, id);
            }
            this.loaded = true;
        }
    }
}
